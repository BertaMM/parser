<?php

namespace MD\Parser\Tests;

use MD\Parser\Parser;
use MD\Parser\Exceptions\ParserException;

class ParserTest extends \PHPUnit\Framework\TestCase
{
	public function testParserWithValidJSONFormat()
    {
        $parser = new Parser();
        $this->assertEquals(
			['name' => 'Glenna Reichert', 'username' => 'Delphine', 'email' => 'glenna.reichert@test.com'],
			$parser->parseToArray(
				'{"name":"Glenna Reichert","username":"Delphine","email":"glenna.reichert@test.com"}',
				"JSON"
			)
		);
    }

	public function testParserisInvalidFormatThrowsException()
    {
		try {
			$parser = new Parser();
	        $parser->parseFromArray(
				'{"name":"Glenna Reichert","username":"Delphine","email":"glenna.reichert@test.com"}',
				null
			);
	    } catch (\Exception $e) {
	        $this->assertInstanceOf(ParserException::class, $e);
	    }
    }

	public function testParserFromArrayWithValidJSONData()
    {
        $parser = new Parser();
        $this->assertEquals(
			'{"name":"Glenna Reichert","username":"Delphine","email":"glenna.reichert@test.com"}',
			$parser->parseFromArray(
				['name' => 'Glenna Reichert', 'username' => 'Delphine', 'email' => 'glenna.reichert@test.com'],
				"JSON"
			)
		);
    }
}
