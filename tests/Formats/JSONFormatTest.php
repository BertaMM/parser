<?php

namespace MD\Parser\Tests\Formats;

use MD\Parser\Formats\JSONFormat;
use MD\Parser\Exceptions\ParserException;

class JSONFormatTest extends \PHPUnit\Framework\TestCase
{
	protected function getJSONData() {
		return '{"name":"Glenna Reichert","username":"Delphine","email":"glenna.reichert@test.com"}';
	}

	protected function getArrayData() {
		return ['name' => 'Glenna Reichert', 'username' => 'Delphine', 'email' => 'glenna.reichert@test.com'];
	}

	public function testJSONFormatterEncodeSuccess()
	{
		$formatter = new JSONFormat();
		$this->assertEquals(
			$this->getArrayData(),
			$formatter->encode($this->getJSONData())
		);
	}

	public function testJSONFormatterDecodeSuccess()
	{
		$formatter = new JSONFormat();
		$this->assertEquals(
			$this->getJSONData(),
			$formatter->decode($this->getArrayData())
		);
	}

	public function testJsonFormatterEmpty()
	{
		$formatter = new JSONFormat();
		$data = $formatter->encode("");
		$this->assertEquals($data, []);
	}
}
