<?php

namespace MD\Parser\Tests\Formats;

use MD\Parser\Formats\XMLFormat;
use MD\Parser\Exceptions\ParserException;

class XMLFormatTest extends \PHPUnit\Framework\TestCase
{
	protected function getXMLData() {
		return '<?xml version="1.0"?><readings><reading clientID="5" name="Taylor Glover" phone="463-170-9623 x156" company="Cargill">Taylor.Glover@gmail.com</reading><reading clientID="8" name="Billy Burnett" phone="493-170-9623 x156" company="Kock Industries">Billy.Burnett@gmail.com</reading></readings>';
	}

	protected function getArrayData() {
		return [
			0 => [
				'reading' => [
					'@attributes' => [
						'clientID'	=> '5',
						'name' => 'Taylor Glover',
						'phone' => '463-170-9623 x156',
						'company' => 'Cargill'
					],
					0 => 'Taylor.Glover@gmail.com'
				]
			],
			1 => [
				'reading' => [
					'@attributes' => [
						'clientID'	=> '8',
						'name' => 'Billy Burnett',
						'phone' => '493-170-9623 x156',
						'company' => 'Kock Industries'
					],
					0 => 'Billy.Burnett@gmail.com'
				]
			],
		];
	}

	public function testXMLFormatterEncodeSuccess()
	{
		$formatter = new XMLFormat();

		$this->assertEquals(
			$this->getArrayData(),
			$formatter->encode($this->getXMLData())
		);
	}

	//TODO
	public function testXMLFormatterDecodeSuccess()
	{
		$formatter = new XMLFormat();
		$this->assertEquals(
			$this->getXMLData(),
			$formatter->decode($this->getArrayData())
		);
	}

	public function testXMLFormatterEmpty()
	{
		$formatter = new XMLFormat();
		$data = $formatter->encode("");
		$this->assertEquals($data, []);
	}
}
