<?php

namespace MD\Parser\Tests\Formats;

use MD\Parser\Formats\CSVFormat;
use MD\Parser\Exceptions\ParserException;

class CSVFormatTest extends \PHPUnit\Framework\TestCase
{
	protected function getCSVData() {
		return [
			"Glenna Reichert,Delphine,glenna.reichert@test.com",
			"Lina Glenn,Pilot Flying,Lina.Glenn@gmail.com"
		];
	}

	protected function getArrayData() {
		return [
			["Glenna Reichert", "Delphine", "glenna.reichert@test.com"],
			["Lina Glenn", "Pilot Flying", "Lina.Glenn@gmail.com"]
		];
	}

	public function testCSVFormatterEncodeSuccess()
	{
		$formatter = new CSVFormat();
		$this->assertEquals(
			$this->getArrayData(),
			$formatter->encode($this->getCSVData())
		);
	}

	public function testCSVFormatterDecodeSuccess()
	{
		$formatter = new CSVFormat();
		$this->assertEquals(
			implode("\n", $this->getCSVData()),
			$formatter->decode($this->getArrayData())
		);
	}
}
