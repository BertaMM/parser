<?php

namespace MD\Parser;

use MD\Parser\Formats\FormatInterface;
use MD\Parser\Exceptions\ParserException;

class Parser {

	protected $allowedFormats = ["JSON", "XML", "CSV"];

	function __construct() {}

	public function parseToArray($data, $format) {

		if(!in_array($format, $this->allowedFormats)) {
			throw new ParserException('The format is not allowed');
		}

		$formatClassname = "MD\\Parser\\Formats\\".$format."Format";
		return $this->encode($data, new $formatClassname());
	}

	public function parseFromArray($data, $format) {

		if(!in_array($format, $this->allowedFormats)) {
			throw new ParserException('The format is not allowed');
		}

		if(!is_array($data)) {
			throw new ParserException('Data must be an array');
		}

		$formatClassname = "MD\\Parser\\Formats\\".$format."Format";
		return $this->decode($data, new $formatClassname());
	}

	protected function encode($data, FormatInterface $format) {
		$result = [];

		try {
			$result = $format->encode($data);
		} catch(\Exception $exception) {
			throw new ParserException('Data can\'t be parsed to Array');
		}

		return $result;
	}

	protected function decode($data = [], FormatInterface $format) {
		$result = null;

		try {
			$result = $format->decode($data);
		} catch(\Exception $exception) {
			throw new ParserException('Data can\'t be parsed to Format');
		}

		return $result;
	}
}
