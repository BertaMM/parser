<?php

namespace MD\Parser\Formats;

class XMLFormat implements FormatInterface
{
	public function decode(array $data) {
		$xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><user_info></user_info>");
		$this->arrayToSimpleXML($data, $xml);

		return $xml;
	}

	public function encode($data) : array {
		if(!$data) return [];
		$xml = simplexml_load_string($data);
		if(!is_iterable($xml)) return [];

		return $this->simpleXMLToArray($xml);
	}

	private function simpleXMLToArray($xml) {
		$result = array();
		foreach ($xml as $element) {
			$attributes = [];
				foreach ($element->attributes() as $attributeName => $attributeValue) {
					$attributes[$attributeName] = (string) $attributeValue;
				}
			$tag = $element->getName();
			$data = get_object_vars($element);
			if (!empty($data)) {
				$result[][$tag] = $element instanceof SimpleXMLElement ? $this->simpleXMLToArray($element) : $data;
			} else {
				$result[][$tag] = trim($element);
			}
		}

		return $result;
	}

	private function arrayToSimpleXML($array, &$xml) {
	    foreach($array as $key => $value) {
	        if(is_array($value)) {
	            if(!is_numeric($key)){
	                $subnode = $xml->addChild("$key");
	                $this->arrayToSimpleXML($value, $subnode);
	            }else{
	                $subnode = $xml->addChild("$key");
	                $this->arrayToSimpleXML($value, $subnode);
	            }
	        }else {
	            $xml->addChild("$key",htmlspecialchars("$value"));
	        }
	    }
	}
}
