<?php

namespace MD\Parser\Formats;

interface FormatInterface
{
    public function decode(array $data);
	public function encode($data) : array;
}
