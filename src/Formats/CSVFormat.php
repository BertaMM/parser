<?php

namespace MD\Parser\Formats;

class CSVFormat implements FormatInterface
{
	public function decode(array $data) {
		return implode("\n", array_map(function($item) {
			return implode(",", $item);
		}, $data));
	}

	public function encode($data) : array {
		if(!$data) return [];
		$csv = array_map('str_getcsv', $data);

		return $csv;
	}
}
