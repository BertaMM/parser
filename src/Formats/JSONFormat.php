<?php

namespace MD\Parser\Formats;

class JSONFormat implements FormatInterface
{
	public function decode(array $data) {
		$result = json_encode($data);

		return $result;
	}

	public function encode($data) : array {
		if(!$data) return [];

		$result = json_decode($data, true);

		return $result;
	}
}
