<?php

namespace MD\Parser\Validators;

interface ValidatorInterface
{
    public function validate($data);
}
